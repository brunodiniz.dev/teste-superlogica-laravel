<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Infos extends Model
{
    use HasFactory;

    protected $table = 'infos';

    protected $fillable = [
        'status_id',
        'info_cpf',
        'info_genero',
        'info_ano_nascimento'
    ];
}
