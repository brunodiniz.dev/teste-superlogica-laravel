<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exec01 extends Model
{
    use HasFactory;

    protected $table = 'exercicio01';

    protected $fillable = [
        'status_id',
        'exec01_name',
        'exec01_username',
        'exec01_zipcode',
        'exec01_email',
        'exec01_password'
    ];

    protected $hidden = [
        'exec01_password'
    ];

}
