<?php

namespace App\Http\Controllers;

use App\Models\Infos;
use App\Models\Usuarios;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class Exec03 extends Controller
{
    protected $modelUsuarios;
    protected $modelInfos;

    public function __construct()
    {
        $this->modelUsuarios = new Usuarios();
        $this->modelInfos = new Infos();
    }

    public function index(){
        return view('exercicio03.index')
            ->with('title', 'Exercício 03 - Superlógica')
            ->with('list', $this->findExec03());
    }

    public function findExec03(){
        try {

            $find = $this->modelUsuarios->select(DB::raw('CONCAT(usuario_nome, " - ", info_genero) AS usuario, IF ((YEAR(NOW()) - info_ano_nascimento) > 50, "SIM", "NÃO") AS maior_50_anos'))
                ->where('usuarios.status_id', 1)
                ->where('info_genero', 'M')
                ->join('infos', 'usuarios.usuario_cpf', '=', 'infos.info_cpf')
                ->orderBy('usuario_cpf')
                ->limit(3)
                ->get();

            return $find;

        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }
}
