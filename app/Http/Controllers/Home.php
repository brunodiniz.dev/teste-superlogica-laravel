<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

class Home extends Controller
{
    public function index(){
        return view('index')->with('title', 'Diniz - Superlógica Laravel');
    }
}
