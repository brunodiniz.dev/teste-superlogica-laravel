<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exec01 as ModelExec01;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;

class Exec01 extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new ModelExec01();
    }

    public function index(){
        return view('exercicio01.index')
            ->with('title', 'Exercício 01 - Superlógica')
            ->with('list', $this->findAll());
    }

    public function findAll(){
        try {

            $find = $this->model
                ->where('exercicio01.status_id', 1)
                ->orderByDesc('exercicio01.exec01_id')
                ->get();

            return $find;

        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    public function store(Request $request){

        $mensagens = [
            'max' => 'O limite de caracteres foi atingido no campo :attribute',
            'required' => 'O :attribute é obrigatório!',
            'name.required' => 'O nome é obrigatório!',
            'name.min' => 'O nome precisa ter no mínimo 3 caracteres',
            'zipCode.required' => 'O CEP é obrigatório!',
            'email.regex' => 'Por favor, digite um e-mail válido',
            'password.regex' => 'A senha deve conter pelo menos 1 letra e 1 número',
            'password.min' => 'A senha deve conter no mínimo 8 caracteres',
            'password.required' => 'A senha é obrigatória!'
        ];

        $request->validate([
            'name' => ['required', 'max:60', 'min:3'],
            'zipCode' => ['required'],
            'email' => ['required', 'max:80', 'regex:([a-zA-Z0-9._][@][a-z0-9].[a-z]{2,3})'],
            'password' => ['required', 'regex:@[a-zA-Z]@', 'regex:@[0-9]@', 'min:8']
        ], $mensagens);
    }

    public function create(Request $request){

        $this->store($request);

        try {
            $exec01 = $this->model::create([
                'status_id' => 1,
                'exec01_name' => $request->name,
                'exec01_username' => strtolower((empty($request->userName))? str_replace(" ", "_", $request->name) : str_replace(" ", "_", $request->userName)),
                'exec01_zipcode' => removeMask($request->zipCode),
                'exec01_email' => $request->email,
                'exec01_password' => trim(password_hash($request->password, PASSWORD_DEFAULT))
            ]);

            Session::flash('success', "O cadastro de $request->name foi inserido com sucesso no ID $exec01->id");
            return redirect('/exercicio01');

        } catch (\Exception $e) {

            return $e->getMessage();
        }

    }

    public function delete(Request $request){
        try {

            $this->model::where('exec01_id', $request->exec01_id)
                ->update(["status_id" => 3]);
            Session::flash('success', "O registro $request->exec01_id foi excluído com sucesso");
            return redirect('/exercicio01');

        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }
}
