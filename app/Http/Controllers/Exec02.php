<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

class Exec02 extends Controller
{
    public function index(){

        return view('exercicio02.index')
            ->with('title', 'Exercício 02 - Superlógica')
            ->with('exercicios', $this->exercicios());

    }

    public function exercicios(){
        $returnFunctions = array();
        $array = array();

        //01
        array_push($returnFunctions, json_encode($array));

        //02
        for($i = 1; $i <= 7; $i++){
            array_push($array, mt_rand(1, 20));
        }
        array_push($returnFunctions, json_encode($array));

        //03
        array_push($returnFunctions, $array[3]);

        //04
        $arrayToStr = implode(',', $array);
        array_push($returnFunctions, $arrayToStr);

        //05
        $stringToArr = array();
        foreach(explode(',', $arrayToStr) as $key => $val){
            array_push($stringToArr, (int) $val);
        }
        unset($array);
        array_push($returnFunctions, json_encode($stringToArr));

        //06
        if(in_array('14', $stringToArr)){
            array_push($returnFunctions, 'Existe!');
        }else{
            array_push($returnFunctions, 'Não existe!');
        }

        //07
        $valorMaior = 0;
        foreach ($stringToArr as $key07 => $val07){

            if(!$key07 == 0){

                if($val07 < $valorMaior){
                    unset($stringToArr[$key07]);
                }else{
                    $valorMaior = $val07;
                }
            }else{
                $valorMaior = $val07;
            }
        }
        array_push($returnFunctions, implode(',', $stringToArr));

        //08
        array_pop($stringToArr);
        array_push($returnFunctions, implode(',', $stringToArr));

        //09
        array_push($returnFunctions, count($stringToArr));

        //10
        array_push($returnFunctions, implode(',', array_reverse($stringToArr)));

        return $returnFunctions;
    }
}
