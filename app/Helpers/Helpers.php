<?php

if(!function_exists('checkEmail')){
    function checkEmail($email){
        if (!preg_match('([a-zA-Z0-9._][@][a-z0-9].[a-z]{2,3})', $email)){
            return false;
        }else{
            $dominio=explode('@',$email);
            if(!checkdnsrr($dominio[1],'A')){
                return false;
            }else{
                return true;
            }
        }
    }
}

if(!function_exists('removeMask')){
    function removeMask($string){
        return preg_replace("/[^0-9a-zA-Z]/", "", $string);
    }
}

if(!function_exists('checkPassword')){
    function checkPassword($password){

        $letra = preg_match('@[a-zA-Z]@', trim($password));
        $numero = preg_match('@[0-9]@', trim($password));

        if (!$letra || !$numero || strlen(trim($password)) < 8){
            echo false;
        }else{
            return true;
        }
    }
}
