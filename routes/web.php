<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home;
use App\Http\Controllers\Exec01;
use App\Http\Controllers\Exec02;
use App\Http\Controllers\Exec03;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Home::class, 'index']);

Route::get('exercicio01', [Exec01::class, 'index']);
Route::post('exercicio01/cadastrar', [Exec01::class, 'create']);
Route::delete('exercicio01/delete/{exec01_id}', [Exec01::class, 'delete'])
    ->where('exec01_id', '[0-9]+');

Route::get('exercicio02', [Exec02::class, 'index']);

Route::get('exercicio03', [Exec03::class, 'index']);
