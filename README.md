<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200"></a></p>

## Siga os passos para o projeto funcionar corretamente

O projeto tem como fomo aprensetar aos responsáveis da entrevista o conhecimento em php!

- Verifique no arquivo .env a conexão com o banco de dados;
- Execute o arquivo data.sql dentro da pasta database em um gerenciador de banco de dados de sua preferência;


Execute os comandos abaixos em um terminal de sua preferência:
- composer install
- php artisan migrate
- php artisan db:seed
- php artisan serve

Após a execução dos comandos o projeto estará funcionando, [clique aqui para acessar](http://127.0.0.1:8000)
