$(document).ready(function(){

    $('.datatable').DataTable({
        "language": {
            "sProcessing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Nada encontrado - desculpe!",
            "sEmptyTable": "Nenhum registro dispoível",
            "info": "Mostrando a página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro disponível",
            "infoFiltered": "(filtrado de _MAX_ registros totais)",
            "sSearch": "Buscar:",
            "sLoadingRecords": "Carregando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Próximo",
                "sPrevious": "Anterior"
            }},
        "order": [[ 0, "desc" ]]
    });

    $("input[name='zipCode']").mask('00000-000');

    $('input').keypress(function (e) {
        var code = null;
        code = (e.keyCode ? e.keyCode : e.which);
        return (code == 13) ? false : true;
    });

    $('body').on('click', '#btnCadastrar', function(){

        Swal.fire({
            title: 'Inserir registro',
            text: "Tem certeza que os dados estão corretos?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não, averiguar'
        }).then((result) => {
            if (result.isConfirmed) {
                $('#submitCadastrar').trigger('click');
            }
        })

    });

    $('body').on('click', "#btnDelete", function(){

        Swal.fire({
            title: 'Deseja excluir o registro',
            icon: 'question',
            text: "Cuidado, pois a exclusão não poderá ser desfeita!",
            showCancelButton: true,
            confirmButtonText: `Sim`,
            cancelButtonText: `Não`
        }).then((result) => {
            if (result.isConfirmed) {
                $('button.btnSubmit-'+$("button#btnDelete").data('id')).trigger('click');
            }
        });
    });

    setTimeout(function(){
        $('div.alert-success').slideUp(800);
    }, 3000);

});
