<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InfosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('infos')->insert([
            'info_cpf' => '16798125050',
            'info_genero' => 'M',
            'info_ano_nascimento' => 1976,
            'status_id' => 1
        ]);

        DB::table('infos')->insert([
            'info_cpf' => '59875804045',
            'info_genero' => 'M',
            'info_ano_nascimento' => 1960,
            'status_id' => 1
        ]);

        DB::table('infos')->insert([
            'info_cpf' => '04707649025',
            'info_genero' => 'F',
            'info_ano_nascimento' => 1988,
            'status_id' => 1
        ]);

        DB::table('infos')->insert([
            'info_cpf' => '21142450040',
            'info_genero' => 'M',
            'info_ano_nascimento' => 1954,
            'status_id' => 1
        ]);

        DB::table('infos')->insert([
            'info_cpf' => '83257946074',
            'info_genero' => 'F',
            'info_ano_nascimento' => 1970,
            'status_id' => 1
        ]);

        DB::table('infos')->insert([
            'info_cpf' => '07583509025',
            'info_genero' => 'M',
            'info_ano_nascimento' => 1972,
            'status_id' => 1
        ]);
    }
}
