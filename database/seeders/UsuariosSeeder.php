<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'usuario_cpf' => '16798125050',
            'usuario_nome' => 'Luke Skywalker',
            'status_id' => 1
        ]);

        DB::table('usuarios')->insert([
            'usuario_cpf' => '59875804045',
            'usuario_nome' => 'Bruce Wayne',
            'status_id' => 1
        ]);

        DB::table('usuarios')->insert([
            'usuario_cpf' => '04707649025',
            'usuario_nome' => 'Diane Prince',
            'status_id' => 1
        ]);

        DB::table('usuarios')->insert([
            'usuario_cpf' => '21142450040',
            'usuario_nome' => 'Bruce Banner',
            'status_id' => 1
        ]);

        DB::table('usuarios')->insert([
            'usuario_cpf' => '83257946074',
            'usuario_nome' => 'Harley Quinn',
            'status_id' => 1
        ]);

        DB::table('usuarios')->insert([
            'usuario_cpf' => '07583509025',
            'usuario_nome' => 'Peter Parker',
            'status_id' => 1
        ]);
    }
}
