<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Exercicio01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercicio01', function (Blueprint $exercicios) {

            $exercicios->charset = 'utf8';
            $exercicios->collation = 'utf8_general_ci';

            $exercicios->increments('exec01_id');
            $exercicios->integer('status_id')->nullable(false)->unsigned();
            $exercicios->string('exec01_name', 60)->nullable(false);
            $exercicios->string('exec01_username', 60)->nullable(false);
            $exercicios->integer('exec01_zipcode')->nullable(false)->unsigned();
            $exercicios->string('exec01_email', 80)->nullable(false);
            $exercicios->text('exec01_password')->nullable(true);
            $exercicios->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->nullable(false);
            $exercicios->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->nullable(false);

            $exercicios->foreign('status_id')->references('status_id')->on('status')->onDelete('cascade')->nullable(false);

            Schema::enableForeignKeyConstraints();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercicio01');
    }
}
