<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Infos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infos', function (Blueprint $infos) {

            $infos->charset = 'utf8';
            $infos->collation = 'utf8_general_ci';

            $infos->increments('info_id');
            $infos->integer('status_id')->nullable(false)->unsigned();
            $infos->bigInteger('info_cpf')->nullable(false);
            $infos->enum('info_genero', ['M', 'F'])->nullable(false);
            $infos->integer('info_ano_nascimento')->nullable(false)->unsigned();
            $infos->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->nullable(false);
            $infos->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->nullable(false);

            $infos->foreign('status_id')->references('status_id')->on('status')->onDelete('cascade')->nullable(false);

            Schema::enableForeignKeyConstraints();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos');
    }
}
