<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Usuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $usuarios) {

            $usuarios->charset = 'utf8';
            $usuarios->collation = 'utf8_general_ci';

            $usuarios->increments('usuario_id');
            $usuarios->integer('status_id')->nullable(false)->unsigned();
            $usuarios->string('usuario_nome', 60)->nullable(false);
            $usuarios->bigInteger('usuario_cpf')->nullable(false);
            $usuarios->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->nullable(false);
            $usuarios->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->nullable(false);

            $usuarios->foreign('status_id')->references('status_id')->on('status')->onDelete('cascade')->nullable(false);

            Schema::enableForeignKeyConstraints();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
