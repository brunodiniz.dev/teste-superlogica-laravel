@include('includes.head')

<header>
    <div class="row">
        <div class="col-12">
            <div class="text-center"><h3>Página Inicial - Superlógica Laravel - Bruno Diniz</h3></div>
        </div>
    </div>
</header>

<div class="row">
    <div class="col-12 text-center">
        <a href="/exercicio01" class="btn btn-secondary"><i class="fas fa-user-plus"></i> Exercício 01</a>
        <a href="/exercicio02" class="btn btn-danger"><i class="fas fa-layer-group"></i> Exercício 02</a>
        <a href="/exercicio03" class="btn btn-warning"><i class="fas fa-table"></i> Exercício 03</a>
    </div>
</div>

@include('includes.footer')
