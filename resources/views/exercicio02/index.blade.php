@include('includes.head')

<header>
    <div class="row">
        <div class="col-6">
            <div class="text-left"><h3>Exercício 02</h3></div>
        </div>
        <div class="col-6">
            <div class="text-right">
                <a href="/" type="button" class="btn btn-dark"> <i class="fas fa-home"></i> Página inicial</a>
            </div>
        </div>
    </div>
</header>

<div class="row">
    <div class="col-12">
        <div class="accordion" id="accordionExample">
            @include('exercicio02.cards')
        </div>
    </div>
</div>

@include('includes.footer')
