@for ($i = 0; $i <= 9; $i++)
    <div class="card">
        <div class="card-header" id="question{{$i}}">
            <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapse{{$i}}" aria-controls="collapse{{$i}}">
                    @switch($i)
                        @case(0)
                        1) Crie um array
                        @break

                        @case(1)
                        2) Popule este array com 7 números
                        @break

                        @case(2)
                        3) Imprima o número da posição 3 do array
                        @break

                        @case(3)
                        4) Crie uma variável com todas as posições do array no formato de string separado por vírgula
                        @break

                        @case(4)
                        5) Crie um novo array a partir da variável no formato de string que foi criada e destrua o array anterior
                        @break

                        @case(5)
                        6) Crie uma condição para verificar se existe o valor 14 no array
                        @break

                        @case(6)
                        7) Faça uma busca em cada posição. Se o número da posição atual for menor que o da posição anterior (valor anterior que não foi excluído do array ainda), exclua esta posição
                        @break

                        @case(7)
                        8) Remova a última posição deste array
                        @break

                        @case(8)
                        9) Conte quantos elementos tem neste array
                        @break

                        @case(9)
                        10) Inverta as posições deste array
                        @break
                    @endswitch
                </button>
            </h2>
        </div>
        <div id="collapse{{$i}}" class="collapse" aria-labelledby="question{{$i}}" data-parent="#accordionExample">
            <div class="card-body">
                @foreach($exercicios as $key => $value)
                    @if($key == $i)
                        {{$value}}
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endfor
