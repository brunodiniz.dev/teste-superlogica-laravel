@include('includes.head')

<header>
    <div class="row">
        <div class="col-6">
            <div class="text-left"><h3>Exercício 01</h3></div>
        </div>
        <div class="col-6">
            <div class="text-right">
                <a href="/" type="button" class="btn btn-dark"> <i class="fas fa-home"></i> Página inicial</a>
            </div>
        </div>
    </div>
</header>

@if ($errors->any())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <i class="fas fa-times"></i>  <span>{{ $error }}</span> <br>
        @endforeach
    </div>
@endif

@if(Session::has('success'))
    <div class="alert alert-success"><i class="fas fa-check-double"></i> {{ session('success') }}</div>
@endif

<div class="row">
    <div class="col-4">
        @include('exercicio01.form')
    </div>

    <div class="col-8">
        @include('exercicio01.table')
    </div>
</div>

@include('includes.footer')
