<form method="post" action="exercicio01/cadastrar">
    @csrf
    <fieldset>Formulário de cadastro</fieldset>
    <div class="form-group">
        <label for="name">Nome completo:</label>
        <input class="form-control" type="text" id="name" name="name" value="{{ old('name') }}">
    </div>
    <div class="form-group">
        <label for="userName">Nome de login:</label>
        <input class="form-control" type="text" id="userName" name="userName" value="{{ old('userName') }}">
    </div>
    <div class="form-group">
        <label for="zipCode">CEP:</label>
        <input class="form-control" type="text" id="zipCode" name="zipCode" value="{{ old('zipCode') }}">
    </div>
    <div class="form-group">
        <label for="email">Email:</label>
        <input class="form-control" type="email" id="email" name="email" aria-describedby="emailInfo" value="{{ old('email') }}">
        <small id="emailInfo" class="form-text text-muted">Digite um e-mail válido.</small>
    </div>
    <div class="form-group">
        <label for="password">Senha:</label>
        <input class="form-control" type="password" id="password" name="password" aria-describedby="passwordInfo">
        <small id="passwordInfo" class="form-text text-muted">8 caracteres mínimo, contendo pelo menos 1 letra
            e 1 número.</small>
    </div>
    <input type="submit" id="submitCadastrar" class="d-none" value="Cadastrar">
    <button type="button" id="btnCadastrar" class="btn btn-success">Cadastrar</button>
</form>
