<table class="datatable table table-striped">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Nome</th>
        <th scope="col">Login</th>
        <th scope="col">CEP</th>
        <th scope="col">E-mail</th>
        <th scope="col">Criação</th>
        <th scope="col">Ações</th>
    </tr>
    </thead>
    <tbody>
    @if(count($list) !== 0)
        @foreach($list as $key => $value)
            <tr>
                <th scope="row">{{$value->exec01_id}}</th>
                <td>{{$value->exec01_name}}</td>
                <td>{{$value->exec01_username}}</td>
                <td>{{ substr_replace($value->exec01_zipcode, '-', 4, 0)}}</td>
                <td>{{$value->exec01_email}}</td>
                <td>{{date('d/m/Y H:m', strtotime(str_replace('-', '/', $value->created_at)))}}</td>
                <td>
                    <button id="btnDelete" data-id="{{$value->exec01_id}}" class="btn btn-danger waves-effect waves-light"><i class="fa fa-times"></i></button>
                    <form action="/exercicio01/delete/{{$value->exec01_id}}" class="d-none" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btnSubmit-{{$value->exec01_id}} btn btn-danger waves-effect waves-light"><i class="fa fa-times"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
