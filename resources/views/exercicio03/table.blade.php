<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">usuario</th>
        <th scope="col">maior_50_anos</th>
    </tr>
    </thead>
    <tbody>
    @if(count($list) !== 0)
        @foreach($list as $key => $value)
            <tr>
                <td>{{$value->usuario}}</td>
                <td>{{$value->maior_50_anos}}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
