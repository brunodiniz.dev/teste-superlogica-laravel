<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="/images/favicon.png"/>
        <title>{{ $title }}</title>

        <!-- Bootstrap 4.6.0 -->
        <link href="/lib/bootstrap-4.6.0/dist/css/bootstrap.css" rel="stylesheet">

        <!-- Font Awesome 5.15.4 -->
        <link href="/lib/fontawesome-5.15.4/css/all.css" rel="stylesheet">

        <!-- DataTables -->
        <link href="/lib/DataTables/datatables.css" rel="stylesheet">

        <!-- Sweetalert 2 -->
        <link href="/lib/sweetalert2-11.1.2/package/dist/sweetalert2.min.css" rel="stylesheet">

        <!-- Estilo Personalizado -->
        <link href="/css/style.css" rel="stylesheet">

    </head>
    <body>
        <div class="container">
