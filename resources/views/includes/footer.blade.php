        </div>

        <footer class="page-footer font-small blue">
            <div class="footer-copyright text-center py-3">© <?php echo date("Y"); ?> Todos os direitos reservados:
                <a href="https://www.linkedin.com/in/diniz-bruno/" target="_blank"> <i class="fab fa-linkedin"></i> Bruno Diniz</a>
            </div>
        </footer>

        <!-- Jquery -->
        <script src="/js/jquery-3.6.0.js"></script>

        <!-- Bootstrap 4.6.0 -->
        <script src="/lib/bootstrap-4.6.0/dist/js/bootstrap.js"></script>

        <!-- Font Awesome 5.15.4 -->
        <script src="/lib/fontawesome-5.15.4/js/all.js"></script>

        <!-- DataTables -->
        <script src="/lib/DataTables/datatables.js"></script>

        <!-- Sweetalert 2 -->
        <script src="/lib/sweetalert2-11.1.2/package/dist/sweetalert2.all.min.js"></script>

        <!-- Jquery Mask -->
        <script src="/lib/jQuery-Mask/dist/jquery.mask.min.js"></script>

        <!-- Javascript Personalizado -->
        <script src="/js/script.js"></script>
    </body>
</hmtl>
